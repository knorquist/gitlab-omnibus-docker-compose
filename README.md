# gitlab-omnibus-docker-compose

This repo provides a quick and simplified method for quickly standing up new GitLab instances with Docker, intended for Support Engineers specifically, and anyone interested in setting up GitLab with Docker Compose generally.

## Installation

1. Stand up your host environment. Note your public IP and **configure your DNS** appropriately. Set `TTL` to `300`. You should have at least your A record pointing to the public IP of your host **before** running `docker compose up -d`, or your SSL cert won't get configured correctly. If this happens, see **Common Issues** below.
1. If you're deploying to AWS, consider instead using `user-data.sh`. This is the quickest way to install Docker and other package dependencies, and clone this repo to `/home/ubuntu/`, the home folder of the default user for Ubuntu machine images. Otherwise, proceed with the below steps.
1. Clone the repo onto the target host with `git clone https://gitlab.com/knorquist/gitlab-omnibus-docker-compose.git`.
1. Install Docker, either manually via Docker's instructions here: [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/), or with the included `setup-docker.sh` file, which is a wrapper script of Docker's official convenience script.
1. Copy `template.env` to `.env` and add your desired variables.
1. If you already have a `Gitlab.gitlab-license`, be sure to include that in the root of the directory, or update `docker-compose.yml` to point to the correct location.
1. When your `.env` is configured and you are satisfied, run `docker compose up -d` to start the container. To check logs, run `docker logs [container name/ID]`.
1. Wait a few minutes while Docker sets up the container and GitLab is installed, and then navigate in your browser to the FQDN you configured earlier.

## Switching GitLab Versions

**If you just want to upgrade the running version of GitLab**, run `docker pull` to get the latest image and then `docker compose up -d`. This will rebuild the container with the latest image.

**If you want to completely re-install GitLab**, run `docker compose down` and then `rm -r` the volumes on the host and re-run `docker compose up -d`. This is useful for when you want to stand up a different version of GitLab. For available versions see here: [GitLab EE on Docker Hub](https://hub.docker.com/r/gitlab/gitlab-ee/tags).


## Making Changes to `gitlab.rb`

The simplest method is to navigate to `$GITLAB_SERVER_HOME/gitlab/config`, make your changes, and then run `docker exec -d [container name/ID] gitlab-ctl reconfigure`.

## Getting to a Rails Console

Run `docker exec -it [container name/ID] gitlab-rails console`. Wait for the Rails console to boot. You'll eventually be met with a splash of info and a `irb(main):001:0>` prompt. Exit with `exit`.

## Registering the Runner

Run `docker run --rm -it -v $GITLAB_RUNNER_HOME/config:/etc/gitlab-runner gitlab/gitlab-runner register`. This will create a short-lived container to register the runner included in `docker-compose.yml`. Follow the prompts - to get the token, create a runner in the web UI.

### Register Non-Interactively

Alternatively, if you've already created the runner in the web UI and have the token ready, run:

```
docker run --rm --env-file .env -v $GITLAB_RUNNER_HOME/config:/etc/gitlab-runner gitlab/gitlab-runner register \
  --non-interactive \
  --url "$EXTERNAL_URL" \
  --token "$RUNNER_TOKEN" \
  --executor "docker" \
  --docker-image alpine:latest \
  --description "docker-runner"
```

If you've updated your `.env` to include the `RUNNER_TOKEN` variable, the above command should pick up that variable and handle everything. Otherwise, remove `--env-file` and add the `EXTERNAL_URL` and `RUNNER_TOKEN` manually.

For documentation see here: [Registering runners](https://docs.gitlab.com/runner/register/index.html?tab=Docker).

## Setting Up GitLab Pages

If you provide `PAGES_EXTERNAL_URL` in your `.env` file, getting Pages working will still require a few more manual steps. Remember, variables loaded via the `GITLAB_OMNIBUS_CONFIG` are loaded at instance startup, but don't actually get inserted into the `gitlab.rb` file.

### Minimum Prerequisites

1. A Pages domain that is not a subdomain of your GitLab instance domain.
1. A wildcard DNS record.
1. A registered shared runner.

Domain examples:
| GitLab Server Domain | GitLab Pages Domain | Does it work? |
| -------------------- | ------------------- | ------------- |
| example.com          | example.dev         | Yes           |
| example.com          | pages.example.com   | No            |
| gitlab.example.com   | pages.example.com   | Yes           |

### Enable Pages

1. Configure your shared runner.
1. Configure your wildcard DNS record to point to your GitLab instance.
1. Edit the `gitlab.rb` file and add the following lines: `pages_external_url "http://your-pages-domain.com"` and `gitlab_pages['enable'] = true`. Reconfigure GitLab with `docker exec -d [container name/ID] gitlab-ctl reconfigure`.

## Common Issues

### SSL Didn't Configure Automatically/No HTTPS

1. Run `docker compose down`
1. `sudo rm -r` the `$GITLAB_SERVER_HOME/gitlab` directory.
1. Make sure your A record is correctly pointing to your public IP.
1. Run `docker compose up -d`.

### Values in `docker-compose.yml` Aren't in `gitlab.rb`

This is a feature, not a bug. Values passed to GitLab via the `GITLAB_OMNIBUS_CONFIG` environment variable are evaluated on startup, so it's useful for jumpstarting things, but not for "pre-building" the `gitlab.rb`. Once your containers are up and GitLab is running, make changes to `gitlab.rb` directly (don't forget to `docker exec -d [container name/ID] gitlab-ctl reconfigure`).

### EC2 Instance Ran Out of Space/500 Errors

Resize the EBS volume in AWS and follow the instructions for extending the file system here: [Extend a Linux file system after resizing a volume](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/recognize-expanded-volume-linux.html).