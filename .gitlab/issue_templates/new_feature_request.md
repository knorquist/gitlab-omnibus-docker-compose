## Summary

<!--- Provide a brief, 3-5 sentence summary of the feature. -->

## Desired Behavior

<!--- What is the desired behavior? -->

## Current Behavior

<!--- Does current functionality address any of your request, even if
partially? -->

## Potential Impact

<!--- How would adding this new feature impact your workflow? Significant/some
efficiency gains? -->

/label ~"new-feature"