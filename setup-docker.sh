#!/bin/bash
# 
# Installs Docker on local system.
# 
# This script wraps around Docker's convenience script, which you can view at:
# https://get.docker.com/
# Instructions for manually installing on Ubuntu are straightforward and can be
# found here: https://docs.docker.com/engine/install/ubuntu/ - if you prefer,
# feel free to follow those instructions.
# 
# This script will also add the current user to the `docker` group, which allows
# you to run Docker commands without having to always use `sudo`. For more
# post-installation steps, see:
# https://docs.docker.com/engine/install/linux-postinstall/
# 
# USAGE
# `chmod +x setup-docker.sh` to make executable, and then
# `sudo ./setup-docker.sh`

set -o nounset
# With `set -o errexit` we will fail fast, and won't reach any error handling.
set -o errexit

function err() {
    echo "[$(date +'%Y-%m-%dT%H:%M:%S%z')]: $*" >&2
}

function check_dir() {
    if [[ "${PWD}" != *"/gitlab-omnibus-docker-compose" ]]; then
        echo "Current dir is ${PWD}, should be 
        .../gitlab-omnibus-docker-compose"
        err "Script initiated in ${PWD}"
        exit 1
    fi
}

function check_sudo() {
    if [[ "${EUID}" != 0 ]]; then
        echo "Please run with sudo."
        err "Script not run as sudo."
        exit 1
    fi
}

function install_docker() {
    curl -fsSL https://get.docker.com -o get-docker.sh
    CurlResp=$?
    if [[ "${CurlResp}" != "0" ]]; then
        err "curl failed with: ${CurlResp}"
        exit 1
    else
        bash ./get-docker.sh
    fi
}

function cleanup(){
    if [[ -f "./get-docker.sh" ]]; then
        rm ./get-docker.sh
    fi
}

function update_docker_group() {
    DockerGroupExists=""
    DockerGroupExists=$(getent group docker)
    if [[ -n "${DockerGroupExists}" ]]; then
        usermod -aG docker "${SUDO_USER}"
    fi
}

check_sudo
check_dir
cleanup
install_docker
update_docker_group

exit 0
