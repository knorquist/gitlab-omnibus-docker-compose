#!/bin/bash
# 
# If you're deploying on an AWS EC2 instance and just want to get up and running
# as quickly as possible, copy-paste or upload this file to the user data under
# "Advanced details". This script assumes an Ubuntu-flavored instance. For more
# info on installing Docker, see: https://docs.docker.com/engine/install/ubuntu/
# 
# Note that user data is run as root on an EC2 instance, so `sudo` is omitted in
# this script. Adjust accordingly if running manually.

# Clean any pre-existing Docker packages. Not necessary on a clean EC2 instance
# normally but included if running on an existing host.
for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do 
    apt remove $pkg; 
done

# Install min packages
apt update
apt install -y curl zip unzip git ca-certificates gnupg
# Add Docker GPG key
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
    | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
chmod a+r /etc/apt/keyrings/docker.gpg

# Add Docker repo to apt sources
echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
    https://download.docker.com/linux/ubuntu \
    $(. /etc/os-release && echo "$VERSION_CODENAME") stable" \
    | tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update

# Install Docker
apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

# Add EC2 default `ubuntu` user to `docker` group so you don't have to use
# `sudo` every time
usermod -aG docker ubuntu

# Pull down the repo into `/home/ubuntu/` to get started faster
cd /home/ubuntu && git clone https://gitlab.com/knorquist/gitlab-omnibus-docker-compose.git
chown -R ubuntu:ubuntu ./gitlab-omnibus-docker-compose